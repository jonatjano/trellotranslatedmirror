const fetch = require("node-fetch")

const validationUtils = require("../src/validationUtils")

const config = require("../config.json")

globalThis.config = config

const trelloToken = config.trelloAuthToken

const organisations = {
    "noOrg": {boards: []}
}

;(async () => {
    await validationUtils.validateTrello()

    fetch(`https://api.trello.com/1/members/me/boards?${globalThis.trello.urlParams}`)
        .then(res => res.json())
        .then(res => {
                res.forEach(board => {
                    const lightBoard = {id: board.id, name: board.name}
                    if (board.idOrganization !== null && board.idOrganization !== undefined) {
                        if (!organisations[board.idOrganization]) {
                            organisations[board.idOrganization] = {boards: []}
                        }
                        organisations[board.idOrganization].boards.push(lightBoard)
                    } else {
                        organisations["noOrg"].boards.push(lightBoard)
                    }
                })

                return Promise.all(Object.keys(organisations).map(orgId =>
                    orgId === "noOrg" ? null : new Promise((resolve, reject) => {
                        fetch(`https://api.trello.com/1/organizations/${orgId}/name?${globalThis.trello.urlParams}`)
                            .then(res => res.status === 200 ? res.json() : {_value: "unauthorized"})

                            // .then(res => res.json())
                            .then(res => {
                                organisations[orgId].name = res._value
                                resolve()
                            })

                            .catch(err => console.error(err) || reject(err))
                    })
                ))
            }
        )
        .then(_ => Object.values(organisations).forEach(
            ({name, boards}) => {
                console.log(`${name} : ${JSON.stringify(boards, null, 4)}`)
            }
        ))
        .catch(err => console.error(err))
})()
