const fetch = require("node-fetch")

const validationUtils = require("../src/validationUtils")

const config = require("../config.json")

global.config = config

const languages = {}

;(async () => {

    await validationUtils.validateDeepL()
    const deeplKey = globalThis.deepL.APIKey

    await fetch(`${globalThis.deepL.url}/languages?auth_key=${deeplKey}&type=target`)
        .then(res => {
            if (res.status === 200) {return res.json()} else {throw new Error(res.status)}
        })
        .then(res =>
            res.forEach(({language, name}) => {
                if (! languages[language]) {languages[language] = {}}
                languages[language].fullName = name
                languages[language].isSource = true
            })
        )
        .catch(err => console.error(err))

    await fetch(`${globalThis.deepL.url}/languages?auth_key=${deeplKey}&type=target`)
        .then(res => {
            if (res.status === 200) {return res.json()} else {throw new Error(res.status)}
        })
        .then(res =>
            res.forEach(({language, name}) => {
                if (! languages[language]) {languages[language] = {}}
                languages[language].fullName = name
                if (languages[language].isSource) {
                    delete languages[language].isSource
                    languages[language].isBoth = true
                } else {
                    languages[language].isTarget = true
                }
            })
        )
        .catch(err => console.error(err))

    console.log(languages)

    const deepLProPlan = ! globalThis.deepL.url.includes("free")
    console.log(`Using a ${deepLProPlan ? "pro" : "free"} account`)
    console.log(`You have used ${globalThis.deepL.charCount}${deepLProPlan ? "" : ` / ${globalThis.deepL.charLimit}`} characters this month`)

})()

