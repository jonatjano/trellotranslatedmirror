const fetch = require("node-fetch")

function isValidTrelloId(id) {
    return /^[0-9a-fA-F]{24}$/.test(id)
}

module.exports = {
    validateConfig: function() {
        if (typeof globalThis.config !== "object") {
            return new Promise((_, rej) => rej("Something unexpected happened, did you create the config.json file ?"))
        }

        if (
            ! isValidTrelloId(globalThis.config.sourceBoard.id) ||
            globalThis.config.targetBoards.some(board => ! isValidTrelloId(board.id))
        ) {
            return new Promise((_, rej) => rej("One board id is not valid"))
        }

        globalThis.config.port = process.env.PORT ?? globalThis.config.port ?? 3000
        if (globalThis.config.serverURL.endsWith("/")) {
            globalThis.config.serverURL = globalThis.config.serverURL.substring(0, globalThis.config.serverURL.length - 1)
        }

        return this.validateAPIKeys()
    },

    validateAPIKeys: function() {
        return Promise.all([this.validateDeepL(), this.validateTrello()])
    },

    validateDeepL: function() {
        const freeUrl = "https://api-free.deepl.com/v2"
        const proUrl = "https://api.deepl.com/v2"
        return fetch(`${freeUrl}/usage?auth_key=${globalThis.config.deepLAPIKey}`)
            .then(res => {
                if (res.status === 200) {
                    if (!globalThis.deepL) {globalThis.deepL = {}}
                    globalThis.deepL.url = freeUrl
                    return res
                } else if (res.status === 403) {
                    return fetch(`${proUrl}/usage?auth_key=${globalThis.config.deepLAPIKey}`)
                        .then(res2 => {
                            if (res2.status === 200) {
                                if (!globalThis.deepL) {globalThis.deepL = {}}
                                globalThis.deepL.url = proUrl
                                return res2
                            } else if (res.status === 403) {
                                throw new Error("DeepL API key is invalid for both free and pro API")
                            } else {
                                // throw res2
                                throw new Error("An unexpected error happened while validating deepL API key (p)")
                            }
                        })
                } else {
                    // throw res
                    throw new Error("An unexpected error happened while validating deepL API key (f)")
                }
            })
            .then(res => res.json())
            .then(res => {
                globalThis.deepL.APIKey = globalThis.config.deepLAPIKey
                globalThis.deepL.charCount = res.character_count
                globalThis.deepL.charLimit = res.character_limit
                globalThis.deepL.urlParams = `auth_key=${globalThis.config.deepLAPIKey}`
            })
            .catch(err => {
                console.error(err.message)
                process.exit(2)
            })
    },

    validateTrello: function() {
        globalThis.trello = {
            url: "https://api.trello.com/1",
            key: "f852fbdb1ebee072098bbd17352d9763"
        }
        return fetch(`${globalThis.trello.url}/tokens/${globalThis.config.trelloAuthToken}?key=${globalThis.trello.key}&token=${globalThis.config.trelloAuthToken}`)
            .then(res => {
                if (res.status === 200) {
                    return res.json()
                } else if (res.status === 401) {
                    throw new Error("Provided trello API token is not valid")
                } else {
                    // throw res
                    throw new Error("An unexpected error happened while validating trello token")
                }
            })
            .then(_ => {
                globalThis.trello.token = globalThis.config.trelloAuthToken
                globalThis.trello.urlParams = `key=${globalThis.trello.key}&token=${globalThis.trello.token}`
            })
            .catch(err => {
                console.error(err.message)
                process.exit(3)
            })
    }
}
