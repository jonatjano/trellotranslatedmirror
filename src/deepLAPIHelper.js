const fetch = require("node-fetch")

/**
 * @callback observerCallback
 * @param {number} charCount the current char count
 */
/**
 * @typedef {string} languageMarker
 * find these by running "npm run listLanguages"
 * they either looks like XX or XX-XX (FR, EN-US, ...)
 */


/**
 * @type {Object.<number, observerCallback>}
 */
observers = {}

function tellObservers(oldCharCount, newCharCount) {
    Object.keys(observers).forEach(step => {
        if (Math.floor(oldCharCount / parseInt(step)) !== Math.floor(newCharCount / parseInt(step))) {
            observers[step].forEach(obs => obs(newCharCount))
        }
    })
}

module.exports = {
    /**
     * @param {string} sentence
     * @param {languageMarker} targetLanguage
     * @return {Promise<string>}
     */
    translate(sentence, targetLanguage) {
        if (globalThis.config.sourceBoard.language === targetLanguage) {
            return new Promise(res => res(sentence))
        }

        const oldCharCount = globalThis.deepL.charCount
        const newCharCount = oldCharCount + sentence.length
        tellObservers(oldCharCount, newCharCount)

        let urlParams = globalThis.deepL.urlParams
        urlParams += `&text=${sentence}`
        if (globalThis.config.sourceBoard.language) { urlParams += `&source_lang=${globalThis.config.sourceBoard.language}` }
        urlParams += `&target_lang=${targetLanguage}`
        urlParams += `&split_sentences=0`

        return fetch(`${globalThis.deepL.url}/translate?${urlParams}`)
            .then(res => {
                if (res.status === 200) {
                    return res.json()
                } else {
                    throw res
                }
            })
            .then(res => res.translations[0].text)
    },

    /**
     * ask for the provided callback to be called everytime the translated character count goes past a multiple of provided step value
     * @param {observerCallback} callback
     * @param {number} step
     */
    observe(callback, step = 10000) {
        if (typeof callback === "function" && typeof step === "number") {
            if (! observers[step]) { observers[step] = []}
            observers[step].push(callback)
        }
    }
}
