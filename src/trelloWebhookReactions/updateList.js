/*
{
  id: '<action id>',
  idMemberCreator: '<member id>',
  data: {
    old: { pos: 196607 },
    list: { pos: 98303, id: '<list id>', name: '<list name>' },
    board: {
      id: '<board id>',
      name: '<board name>',
      shortLink: '<board short link>'
    }
  },
  type: 'updateList',
  date: '2021-05-07T11:30:41.960Z',
  appCreator: null,
  limits: {},
  display: {
    translationKey: 'action_moved_list_left',
    entities: {
      list: {
        type: 'list',
        pos: 98303,
        id: '<list id>',
        text: '<list name>'
      },
      board: {
        type: 'board',
        id: '<board id>',
        text: '<board name>',
        shortLink: '<board short link>'
      },
      memberCreator: {
        type: 'member',
        id: '<member id>',
        username: '<member username>',
        text: '<member name>'
      }
    }
  },
  memberCreator: {
    id: '<member id>',
    username: '<member username>',
    activityBlocked: false,
    avatarHash: '<member avatar hash>',
    avatarUrl: 'https://trello-members.s3.amazonaws.com/<member id>/<member avatar hash>',
    fullName: '<member name>',
    idMemberReferrer: null,
    initials: '.',
    nonPublic: {},
    nonPublicAvailable: true
  }
}
 */


/*
{
  id: '<action id>',
  idMemberCreator: '<member id>',
  data: {
    old: { pos: 98303 },
    list: { pos: 196607, id: '<list id>', name: '<list name>' },
    board: {
      id: '<board id>',
      name: '<board name>',
      shortLink: '<board short link>'
    }
  },
  type: 'updateList',
  date: '2021-05-07T11:35:40.448Z',
  appCreator: null,
  limits: {},
  display: {
    translationKey: 'action_moved_list_right',
    entities: {
      list: {
        type: 'list',
        pos: 196607,
        id: '<list id>',
        text: '<list name>'
      },
      board: {
        type: 'board',
        id: '<board id>',
        text: '<board name>',
        shortLink: '<board short link>'
      },
      memberCreator: {
        type: 'member',
        id: '<member id>',
        username: '<member username>',
        text: '<member name>'
      }
    }
  },
  memberCreator: {
    id: '<member id>',
    username: '<member username>',
    activityBlocked: false,
    avatarHash: '<member avatar hash>',
    avatarUrl: 'https://trello-members.s3.amazonaws.com/<member id>/<member avatar hash>',
    fullName: '<member name>',
    idMemberReferrer: null,
    initials: '.',
    nonPublic: {},
    nonPublicAvailable: true
  }
}
 */

/*
{
  id: '<action id>',
  idMemberCreator: '<member id>',
  data: {
    old: { name: '<list old name>' },
    list: { name: '<list new name>', id: '<list id>' },
    board: {
      id: '<board id>',
      name: '<board name>',
      shortLink: '<board short link>'
    }
  },
  type: 'updateList',
  date: '2021-05-07T11:37:19.864Z',
  appCreator: null,
  limits: {},
  display: {
    translationKey: 'action_renamed_list',
    entities: {
      list: { type: 'list', id: '<list id>', text: '<list new name>' },
      name: { type: 'text', text: '<list old name>' },
      memberCreator: {
        type: 'member',
        id: '<member id>',
        username: '<member username>',
        text: '<member name>'
      }
    }
  },
  memberCreator: {
    id: '<member id>',
    username: '<member username>',
    activityBlocked: false,
    avatarHash: '<member avatar hash>',
    avatarUrl: 'https://trello-members.s3.amazonaws.com/<member id>/<member avatar hash>',
    fullName: '<member name>',
    idMemberReferrer: null,
    initials: '.',
    nonPublic: {},
    nonPublicAvailable: true
  }
}
 */

/*{
  id: '<action id>',
  idMemberCreator: '<member id>',
  data: {
    old: { closed: false },
    list: { closed: true, id: '<list id>', name: '<list name>' },
    board: {
      id: '<board id>',
      name: '<board name>',
      shortLink: '<board short link>'
    }
  },
  type: 'updateList',
  date: '2021-05-07T12:07:40.112Z',
  appCreator: null,
  limits: {},
  display: {
    translationKey: 'action_archived_list',
    entities: {
      list: {
        type: 'list',
        closed: true,
        id: '<list id>',
        text: '<list name>'
      },
      memberCreator: {
        type: 'member',
        id: '<member id>',
        username: '<member username>',
        text: '<member name>'
      }
    }
  },
  memberCreator: {
    id: '<member id>',
    username: '<member username>',
    activityBlocked: false,
    avatarHash: '<member avatar hash>',
    avatarUrl: 'https://trello-members.s3.amazonaws.com/<member id>/<member avatar hash>',
    fullName: '<member name>',
    idMemberReferrer: null,
    initials: '.',
    nonPublic: {},
    nonPublicAvailable: true
  }
}
 */

const util = require("util");

module.exports = action => {
    console.warn("WIP : updateList action type")
    console.warn(util.inspect(action, false, null, true))
}
