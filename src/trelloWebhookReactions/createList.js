/*
{
  id: '<action id>',
  idMemberCreator: '<member id>',
  data: {
    list: { id: '<new list id>', name: '<new list name>' },
    board: {
      id: '<board id>',
      name: '<board name>',
      shortLink: '<board short link>'
    }
  },
  type: 'createList',
  date: '2021-05-07T11:04:16.999Z',
  appCreator: null,
  limits: {},
  display: {
    translationKey: 'action_added_list_to_board',
    entities: {
      list: {
        type: 'list',
        id: '<new list id>',
        text: '<new list name>'
      },
      board: {
        type: 'board',
        id: '<board id>',
        text: '<board name>',
        shortLink: '<board short link>'
      },
      memberCreator: {
        type: 'member',
        id: '<member id>',
        username: '<member username>',
        text: '<member name>'
      }
    }
  },
  memberCreator: {
    id: '<member id>',
    username: '<member username>',
    activityBlocked: false,
    avatarHash: '<member avatar hash>',
    avatarUrl: 'https://trello-members.s3.amazonaws.com/<member id>/<member avatar hash>',
    fullName: '<member name>',
    idMemberReferrer: null,
    initials: '.',
    nonPublic: {},
    nonPublicAvailable: true
  }
}
 */

const deepLApiHelper = require("../deepLAPIHelper");
const trelloAPIHelper = require("../trelloAPIHelper");

module.exports = action => {
    globalThis.config.targetBoards.forEach(async board => {
        const listName = await deepLApiHelper.translate(action.data.list.name, board.language)

        trelloAPIHelper.list.create(listName, board.id)
            .then(() => console.log("success"))
            .catch(async err => console.error(await err))
    })
}
