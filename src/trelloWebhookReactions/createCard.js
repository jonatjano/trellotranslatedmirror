/*
{
  id: '<action id>',
  idMemberCreator: '<member id>',
  data: {
    card: {
      id: '<card id>',
      name: '<card name>',
      idShort: 1,
      shortLink: '<card short link>'
    },
    list: { id: '<list id>', name: '<list name>' },
    board: {
      id: '<board id>',
      name: '<board name>',
      shortLink: '<board short id>'
    }
  },
  type: 'createCard',
  date: '2021-05-07T11:37:59.514Z',
  appCreator: null,
  limits: {},
  display: {
    translationKey: 'action_create_card',
    entities: {
      card: {
        type: 'card',
        id: '<card id>',
        shortLink: '<card short link>',
        text: '<card name>'
      },
      list: { type: 'list', id: '<list id>', text: '<list name>' },
      memberCreator: {
        type: 'member',
        id: '<member id>',
        username: '<member username>',
        text: '<member name>'
      }
    }
  },
  memberCreator: {
    id: '<member id>',
    username: '<member username>',
    activityBlocked: false,
    avatarHash: '<member avatar hash>',
    avatarUrl: 'https://trello-members.s3.amazonaws.com/<member id>/<member avatar hash>',
    fullName: '<member name>',
    idMemberReferrer: null,
    initials: '.',
    nonPublic: {},
    nonPublicAvailable: true
  }
}
 */

const util = require("util");

module.exports = action => {
    console.warn("WIP : createCard action type")
    console.warn(util.inspect(action, false, null, true))
}
