const fetch = require("node-fetch")

module.exports = {
    list: {
        create(name, idBoard) {
            console.warn("WIP : trelloAPIHelper.createList")
            let urlParams = globalThis.trello.urlParams
            urlParams += `&name=${name}`
            urlParams += `&idBoard=${idBoard}`
            urlParams += `&pos=bottom`
            return fetch(`${globalThis.trello.url}/lists?${urlParams}`, {method: "post"})
                .then(res => {
                    if (res.status === 200) {
                        return res
                    } else {
                        throw res.text()
                    }
                })
        }
    }
}
