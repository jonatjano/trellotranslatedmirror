const util = require("util");

const express = require("express")
const fetch = require("node-fetch")

const config = require("./config.json")

globalThis.config = config

const router = express.Router()
router.use(express.json())

/**
 * @type {Promise<express.Router>}
 */
module.exports = {
    get router() {
        router.head("/trelloWebhook", function(req, res) {
            return res.status(200).end()
        })

        router.post("/trelloWebhook", function(req, res) {

            if (req.body.model.id === global.config.sourceBoard.id) {

                // thing get cached after first require so no overhead here
                const reaction = require(`./src/trelloWebhookReactions/${req.body.action.type}`)

                if (typeof reaction === "function") {
                    reaction(req.body.action)
                } else {
                    console.warn("Unregistered action type")
                    console.warn(util.inspect(req.body.action, false, null, true))
                }
            }

            return res.status(200).end()
        })

        return router
    },

    listenCallback() {
        return new Promise(async res => {

            await require("./src/validationUtils").validateConfig()
                .catch(err => {
                    console.error(err)
                    process.exit(4)
                })

            const deepLApi = require("./src/deepLAPIHelper")
            const trelloApi = require("./src/trelloAPIHelper")

            await fetch(`${globalThis.trello.url}/tokens/${globalThis.trello.token}/webhooks?${globalThis.trello.urlParams}`)
                .then(res => {
                    if (res.status === 200) {
                        return res.json()
                    }
                    throw res
                })
                .then(res => console.log(res) || ! res.some(webhook => webhook.idModel === globalThis.config.sourceBoard.id) )
                .then(shouldCreateHook => {
                    if (shouldCreateHook) {
                        let urlParams = globalThis.trello.urlParams
                        urlParams += `&description=automated%20translation%20webhook`
                        urlParams += `&callbackURL=${globalThis.config.serverURL}/trelloWebhook`
                        urlParams += `&idModel=${globalThis.config.sourceBoard.id}`
                        return fetch(`${globalThis.trello.url}/webhooks?${urlParams}`, {method: "post"})
                            .then(webhookRes => {
                                if (webhookRes.status === 200) {
                                    return webhookRes.json()
                                }
                                throw webhookRes
                            })
                            .then(webhook => {
                                console.log(`successfully create webhook for board ${globalThis.config.sourceBoard.id}`)
                            })
                    } else {
                        console.log(`webhook for board ${globalThis.config.sourceBoard.id} already exists`)
                    }
                })
                .catch(async err => {
                    console.error(`Failed to get existing trello webhooks for token : ${await err.text()}`)
                    process.exit(5)
                })
        })
    }
}

