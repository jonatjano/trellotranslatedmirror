const express = require("express")
const app = express()

const router = require("./router")

;(async ()=>{
    app.use("/", router.router)

    app.listen(globalThis.config.port, () => {
        console.log(`Listening for trello webhooks on port ${globalThis.config.port}`)
        router.listenCallback()
    })
})()
