# trelloTranslatedMirror

Automatically translate a trello board into other boards

## set up
### create accounts
Before starting you need to create a trello account as well as a DeepL account.

### create config file
Duplicate `configTemplate.json` into a new file named `config.json` and replace the placeholders with your actual configurations

#### config file format
the config file is a `JSON` file, it contains an object with the following entries :
- `serverURL`: the url of the server this program will be running on
- `port`: port used for the http server which listen to trello webhook
- `trelloAuthToken`: your trello auth token, get it on https://trello.com/app-key
- `deepLAPIKey`: your deepL API key, get it on https://www.deepl.com/pro-account/plan
- `sourceBoard`: a `board info` containing the `id` and `language` of the board to be translated. `language` is optional, if absent deepL will try to detect it
- `targetBoards`: an array of `boards info` containing the information on the target boards

each `boards info` object contains the following entries:
- `id`: output board id, use 'npm run listBoards' to list the boards linked to your account (needs config.trelloAuthToken to be set)
- `language`: two (or four) letter code of language, get it with 'npm run listLanguages' (needs config.deepLAPIKey to be set)

**WARNING** : output boards content **WILL** be wiped on first launch (and if you delete the `state.json` file), each modification you do on them will be **lost** if you modify the same field in the source board

